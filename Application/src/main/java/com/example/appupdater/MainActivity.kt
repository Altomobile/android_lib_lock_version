package com.example.appupdater

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import io.altomobile.updater.Updater

class MainActivity
    : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkForUpdate()
    }

    private fun checkForUpdate() {
        Updater(
            this,
            "https://www.google.com/ncr",
            BuildConfig.VERSION_NAME,
            getString(R.string.app_name),
            "There's a new update, this version might not work properly if it's not updated.",
            "Failed to open the link to install the app, please do it manually."
        ) {

        }.checkUpdate()
    }

}
