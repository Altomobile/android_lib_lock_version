package io.altomobile.updater

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.net.URL

class Updater(
    private val context: Context,
    private val versionsUrl: String,
    private val currentVersion: String,
    private val appName: String,
    private val updateMessage: String,
    private val failedToLaunchInstall: String,
    private val onNoUpdated: () -> Unit
) {

    fun checkUpdate() {
        doAsync {
            val url = URL(versionsUrl)
            val json = url.readText()
            val versions: List<String> = Gson().fromJson(json, object : TypeToken<ArrayList<String>>() {}.type)

            var blocked = false
            versions.forEach {
                if (currentVersion.compareTo(it) == 0) {
                    blocked = true
                }
            }

            uiThread {
                if (blocked) {
                    showUpdateDialog()
                } else {
                    onNoUpdated()
                }
            }
        }
    }

    private fun showUpdateDialog() {
        AlertDialog.Builder(context)
            .setTitle(appName)
            .setMessage(updateMessage)
            .setPositiveButton(android.R.string.ok, null)
            .setCancelable(false)
            .create()
            .apply {
                setCancelable(false)
                setCanceledOnTouchOutside(false)
                setOnShowListener {
                    val view: View = getButton(AlertDialog.BUTTON_POSITIVE)
                    view.setOnClickListener {
                        openStoreForUpdate()
                    }
                }
                show()
            }
    }

    private fun openStoreForUpdate() {
        var intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse("market://details?id=${context.packageName}")
        )
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        if (intent.resolveActivity(context.packageManager) != null) {
            context.startActivity(intent)
        } else {
            intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=${context.packageName}")
            )
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            if (intent.resolveActivity(context.packageManager) != null) {
                context.startActivity(intent)
            } else {
                AlertDialog.Builder(context)
                    .setTitle(appName)
                    .setMessage(failedToLaunchInstall)
                    .setPositiveButton(android.R.string.ok) { dialog, _ -> dialog.dismiss() }
                    .create()
                    .show()
            }
        }
    }

}
